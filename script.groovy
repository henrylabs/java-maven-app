def buildJar(){
    echo 'building the application'
    sh 'mvn package'
}

def buildImage(){
    echo "Building the docker image"
    withCredentials([usernamePassword(credentialsId: 'docker-hub-login', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
    sh 'docker build -t henryroman/java-maven-app:jma-2.0 .'
    sh "echo $PASS | docker login -u $USER --password-stdin"
    sh 'docker push henryroman/java-maven-app:jma-2.0'
    }
}

def deploy(){
    echo 'deploying application'
}

return this
